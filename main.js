/**
 * Wikipedia-scraper
 * 
 * Miika Kulmala 2021
 * 
 * Tested on NodeJs v14.17.1
 */

import axios from "axios";
import { parse } from 'node-html-parser';
import * as readline from 'readline'
import { stdin as input, stdout as output } from 'process';

const rl = readline.createInterface({ input, output });

const API = 'https://en.wikipedia.org/api/rest_v1/page/html/';

// App class handles user inputs.
class App {
  constructor() {
    // Defaults
    this.startLink = 'Random'
    this.depth = 3;
    this.count = 2;

    this.askProperties();
  }

  async startScrape() {
    try {
      const scraper = new Scraper(this.startLink, this.depth, this.count);
      await scraper.start()
    } catch (err) {
      console.error('Scraper: ' + err);
    }
    process.exit(0);
  }

  // Handles user-input-flow
  async askProperties() {
    try {
      this.startLink = await this.askTitle();
      this.depth = await this.askNumber('Input how many links you want to follow: ')
      this.count = await this.askNumber('Input how many links you want to see per article: ')
      this.startScrape();
    } catch (err) {
      console.error(err)
      this.askProperties()
    }
  }

  askTitle = () => new Promise((resolve) => {
    rl.question('Input article name: (Leave empty for random) ', (ans) => {
      if (ans !== '') resolve(ans)
      else resolve(this.startLink)
    })
  })


  askNumber = (question) => new Promise((resolve, reject) => {
    rl.question(question, ans => {
      if (isNaN(ans) || !parseInt(ans) || parseInt(ans) < 1) {
        reject(ans + ' is not valid')
      } else {
        resolve(parseInt(ans));
      }
    })

  })
}

// Scraper class handles requesting and formatting of data.
class Scraper {
  constructor(startLink, depth, count) {
    this.startLink = startLink
    this.depth = depth;
    this.count = count;

    this.structured = null;
  }

  async start() {
    // Initial structure
    this.structured = { label: 'root', children: [{ label: this.startLink, children: null }] }

    await this.recursiveLinks(this.structured, this.depth);
    this.printStructureRecursive(this.structured.children[0])
  }

  // Api request to wikimedia api. Parses response with node-html-parser.
  async requestDomFromTitle(label) {
    try {
      const { data } = await axios.get(API + label)
      const parsedDom = parse(data);
      return parsedDom;

    } catch (err) {
      const { status } = err.response;
      if (status === 404) throw new Error('Article not found')
      if (status === 400) throw new Error('Too many requests')
      else throw new Error(err)
    }
  }

  // Fires request for fetching article and runs data through filtering functions.
  async getLinks(title) {
    const dom = await this.requestDomFromTitle(title);
    return await this.getTitlesfromDom(dom, title);
  }

  // Searches DOM-tree for links and runs filtering for found items
  async getTitlesfromDom(dom, title) {
    const body = dom.getElementsByTagName('body')[0];
    const links = body.getElementsByTagName('a');
    return this.filterLinks(links, this.count, title)
  }

  // Handles filtering data from dom. Returns only array containing (count) number of links
  filterLinks(links, count, title) {
    let seen = [];
    const filtered = links.filter(link => {
      const linkTitle = link.getAttribute('title');
      if (
        // Only display links
        link.getAttribute('rel') !== 'mw:WikiLink' ||
        // Filters out help-, template-pages etc.
        linkTitle?.includes(':') ||
        // Filters disambiguations. Gives more info and seemed like in the example those were also filtered
        linkTitle?.includes('(disambiguation)') ||
        // check that article is not same as parent
        linkTitle === title ||
        // Check if duplicate
        seen.includes(linkTitle)
      ) {
        return false
      } else {
        seen.push(linkTitle)
        return true
      }
    })

    return filtered.slice(0, count).map(link => trimPath(link.getAttribute('href')))
  }

  // Handles saving new data to data-structure
  saveToStructure(arr, parentObj) {
    parentObj.children = arr.map(link => ({ label: link, children: null }))
    return parentObj.children;
  }

  // Recursively fires requests for needed articles and saves them to data-structure
  async recursiveLinks(parent, maxDepth, depth = 0) {
    if (depth < maxDepth) {
      for (const child of parent.children) {
        const newArr = await this.getLinks(child.label)
        this.saveToStructure(newArr, child);

        await this.recursiveLinks(child, maxDepth, depth + 1);
      }
    }
  }

  // Traverses data-structure and prints labels
  printStructureRecursive(parent, depth = 0) {
    console.log('  '.repeat(depth) + parent.label);
    if (parent.children) {
      parent.children.forEach(child => this.printStructureRecursive(child, depth + 1));
    }
  }
}

// Helper functions
const trimPath = path => path.substring(2);

// Start
const app = new App();